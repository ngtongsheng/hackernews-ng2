# Hacker news clone site using Angular 2

This is a [hacker news](https://news.ycombinator.com/) clone site using angular 2, this project is for learning purpose only, using Algolia powered [api](https://hn.algolia.com/api). The site is hosted at [hn.bojio.me](http://hn.bojio.me).

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

## What you need
For this project you need [nodejs](https://nodejs.org/en/), visit the link to download and install.

After installed node, install Angular cli
```bash
npm install -g angular-cli
```

## After clone

After successfully cloned the site to your local machine, run:
```bash
npm install
```


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
