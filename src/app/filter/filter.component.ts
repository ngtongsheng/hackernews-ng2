import { Component, Input, Output, EventEmitter } from '@angular/core';

const template = require("pug!./filter.component.pug");

@Component({
  selector: 'filter',
  template: template(),
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent {

  sortBy: string;
  @Output() change = new EventEmitter();

  @Input()
  get sort(): string {
    return this.sortBy;
  }

  set sort(val) {
    this.sortBy = val;
    this.change.emit(val);
  }
}
