import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { DateService } from '../shared/date.service';


@Injectable()
export class HnAPIService {
  private url = 'http://hn.algolia.com/api/v1/';

  public SORT_BY_DATE: string = 'search_by_date';
  public SORT_BY_POPULARITY: string = 'search';
  public HITS_PER_PAGE: number = 20;

  constructor(
    private httpService: HttpService,
    private dateUtil: DateService,
  ) { }

  public show(page: number = 0, sort = this.SORT_BY_POPULARITY, search: string) {
    return this.getSearchApi({ tag: 'show_hn', page, sort, search });
  };

  public ask(page: number = 0, sort = this.SORT_BY_POPULARITY, search: string) {
    return this.getSearchApi({ tag: 'ask_hn', page, sort, search });
  };

  public hot(page: number = 0, sort = this.SORT_BY_POPULARITY, search: string) {
    return this.getSearchApi({ tag: 'story', page, sort, search });
  };

  public comment(page: number = 0, sort = this.SORT_BY_POPULARITY, search: string) {
    return this.getSearchApi({ tag: 'comment', page, sort, search });
  };

  public user = (name: string) => this.httpService.get(this.getUserApiUrl(name));

  public item = (id: string) => {
    return this.httpService.get(this.getItemApiUrl(id))
      .map(item => {
        const isComment = (item.comment_text !== null);

        const url = this.ifNotAThenB(item.story_url, item.url);
        const link = url ? url : null;
        const itemRoute = ['/item', id];
        const title = this.ifNotAThenB(item.story_title, item.title);

        item.link = link;
        item.title = title;
        item.itemRoute = itemRoute;
        item.thumb = `https://drcs9k8uelb9s.cloudfront.net/${id}.png`;
        item.domain = url ? this.getDomain(url) : '';
        item.comment = item.num_comments ? item.num_comments : '';
        item.created = this.dateUtil.getDayAgo(new Date(item.created_at));

        return item;
      })
  };

  private getSearchApi = (params) => {
    const url = this.getSearchApiUrl(params);

    return this.httpService.get(url)
      .map(({hits, nbPages, page, hitsPerPage}) => {
        hits = hits.map(this.mapHits(hitsPerPage, page));
        return {
          hits,
          count: nbPages,
          perPage: hitsPerPage,
          page
        }
      });
  }

  private mapHits = (perPage, page) => {
    return (item, index) => {
      const isComment = (item.comment_text !== null);

      const id = this.ifNotAThenB(item.story_id, item.objectID);
      const url = this.ifNotAThenB(item.story_url, item.url);
      const link = url ? url : null;
      const itemRoute = ['/item', id];
      const title = this.ifNotAThenB(item.story_title, item.title);
      const comment = `${item.num_comments} comment${item.num_comments > 1 ? 's' : ''}`;

      item.index = (perPage * page) + index + 1;
      item.link = link;
      item.title = `${item.index}. ${title}`;
      item.itemRoute = itemRoute;
      item.thumb = `https://drcs9k8uelb9s.cloudfront.net/${id}.png`;
      item.domain = url ? this.getDomain(url) : '';
      item.comment = item.num_comments !== null ? comment : item.num_comments;
      item.created = this.dateUtil.getDayAgo(new Date(item.created_at));

      return item;
    }
  }

  private getSearchApiUrl = ({ tag, sort, page, search = '' }) => {
    return `${this.url + sort}?hitsPerPage=${this.HITS_PER_PAGE}&query=${search}&tags=${tag}&page=${page}`;
  }

  private getApiUrl = (apiName, param) => `${this.url}${apiName}/${param}`;

  private getUserApiUrl = (name) => this.getApiUrl('users', name);

  private getItemApiUrl = (id) => this.getApiUrl('items', id);

  private getDomain = (url) => {
    const split = url.split('/');
    const removePort = domain => domain.split(':')[0];

    if (url.indexOf("://") > -1) {
      return removePort(split[2]);
    }

    return removePort(split[0]);
  }

  private ifNotAThenB = (a, b) => a ? a : b;
}