import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class HttpService {
  constructor(private http: Http) { }

  public get(url) {
    const api = this.http.get(url);
    return this.httpJson(api);
  }

  private httpJson = observable => observable.do(this.consoleResponse).map(this.resToJson).do(this.console);

  private consoleResponse = res => {
    const { url, status } = res;
    console.log(`status: ${status}, url: ${url}`);
  };

  private console = res => console.log(res);

  private resToJson = res => res.json();
}