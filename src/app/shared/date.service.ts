import { Injectable } from '@angular/core';

@Injectable()
export class DateService {
  constructor() { }

  getDayAgo(from: Date, to: Date = new Date()): String {
    const timeDiff = this.getTimeDiff(from, to);

    const yearDiff = this.getYearDiff(timeDiff);
    const monthDiff = this.getMonthDiff(timeDiff);
    const weekDiff = this.getWeekDiff(timeDiff);
    const dayDiff = this.getDayDiff(timeDiff);
    const minDiff = this.getMinuteDiff(timeDiff);

    const years = `year${this.getPluralS(yearDiff)}`;
    const months = `month${this.getPluralS(monthDiff)}`;
    const weeks = `week${this.getPluralS(weekDiff)}`;
    const days = `day${this.getPluralS(dayDiff)}`;
    const mins = `min${this.getPluralS(minDiff)}`;

    if (minDiff === 0) {
      return 'a moment ago';
    }

    if (dayDiff === 0) {
      return `${minDiff} ${mins} ago`;
    }

    if (weekDiff === 0) {
      return `${dayDiff} ${days} ago`;
    }

    if (monthDiff === 0) {
      return `${weekDiff} ${weeks} ago`;
    }

    if (yearDiff === 0) {
      return `${monthDiff} ${months} ago`;
    }

    return `${yearDiff} ${years} ago`;
  }

  getPluralS = (num) => num > 1 ? 's' : '';

  getTimeDiff = (from, to) => Math.abs(to.getTime() - from.getTime());

  getYearDiff = (timeDiff) => Math.floor(timeDiff / (1000 * 3600 * 24 * 365));

  getMonthDiff = (timeDiff) => Math.floor(timeDiff / (1000 * 3600 * 24 * 30));

  getWeekDiff = (timeDiff) => Math.floor(timeDiff / (1000 * 3600 * 24 * 7));

  getDayDiff = (timeDiff) => Math.floor(timeDiff / (1000 * 3600 * 24));

  getMinuteDiff = (timeDiff) => Math.floor(timeDiff / (1000 * 3600));
}