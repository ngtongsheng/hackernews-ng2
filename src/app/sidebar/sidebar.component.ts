import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

const template = require("pug!./sidebar.component.pug");

@Component({
  selector: 'sidebar',
  template: template(),
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {

  navs: any[];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.navs = [
      {
        tag: 'hot',
        icon: 'hot_tub'
      },
      {
        tag: 'show',
        icon: 'movie_filter'
      },
      {
        tag: 'ask',
        icon: 'speaker_notes'
      },
      {
        tag: 'comment',
        icon: 'insert_chart'
      }
    ];
  }

  isActive = (target) => (this.router.url.indexOf('/' + target.tag) !== -1);
}
