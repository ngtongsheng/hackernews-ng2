import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HnAPIService } from '../shared/hn.api.service';
import { Observable } from 'rxjs/';

const template = require("pug!./stories.component.pug");

@Component({
  selector: 'stories',
  template: template(),
  styleUrls: ['./stories.component.scss']
})

export class StoriesComponent implements OnInit {

  title: string;
  tag: string;
  count: number;
  page: number;
  stories: any[];
  search: string = '';
  isLoading: boolean = false;
  sort: string = 'search';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hnAPI: HnAPIService,
  ) { }

  ngOnInit() {
    this.getTitle();
    this.getPage();
  }

  getTitle() {
    this.route.data
      .subscribe(this.onGetTitle);
  }

  onGetTitle = ({ title, tag }) => {
    this.title = title;
    this.tag = tag;
  };

  getPage() {
    const onMapPage = page => (page as any).page;
    const timerAudit = () => Observable.timer(500, 1);

    this.route.params
      .audit(timerAudit)
      .map(onMapPage)
      .subscribe(this.onGetPage);
  }

  onGetPage = page => {
    this.page = page;
    return this.getStories(page);
  };

  getStories(page) {
    this.isLoading = true;
    const getStoriesAPI = page => this.hnAPI[this.tag](page - 1, this.sort, this.search);

    return getStoriesAPI(page)
      .subscribe(this.onGetStories);
  }

  onGetStories = stories => {
    this.stories = stories.hits;
    this.count = stories.count;
    this.isLoading = false;
  };

  onPageChange = page => {
    this.page = page;
    this.router.navigate(['/' + this.tag, page]);
  };

  onSearch = search => {
    if (!search || !this.page) {
      return;
    }

    this.search = search;

    if (search !== '' && this.page !== 1) {
      this.onPageChange(1);
      return;
    }

    this.getStories(this.page);
  }

  onSort = (sort) => {
    if ((sort && sort.type) || !this.page) {
      return;
    }

    this.sort = sort;
    this.getStories(this.page);
  };
}
