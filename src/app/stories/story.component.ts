import { Component, Input } from '@angular/core';

const template = require("pug!./story.component.pug");

@Component({
  selector: 'story',
  template: template(),
  styleUrls: ['./story.component.scss']
})

export class StoryComponent {
  @Input() story;
}
