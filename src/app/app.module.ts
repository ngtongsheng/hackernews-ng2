import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { routing } from './app.route';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { HnAPIService } from './shared/hn.api.service';
import { HttpService } from './shared/http.service';
import { DateService } from './shared/date.service';
import { StoriesComponent } from './stories/stories.component';
import { StoryComponent } from './stories/story.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SearchComponent } from './search/search.component';
import { FilterComponent } from './filter/filter.component';
import { ItemComponent } from './item/item.component';
import { CommentComponent } from './comment/comment.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    StoriesComponent,
    StoryComponent,
    SidebarComponent,
    SpinnerComponent,
    PaginationComponent,
    SearchComponent,
    FilterComponent,
    ItemComponent,
    CommentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    routing
  ],
  providers: [HttpService, HnAPIService, DateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
