import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HnAPIService } from '../shared/hn.api.service';

const template = require("pug!./item.component.pug");

@Component({
  selector: 'app-item',
  template: template(),
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  item;

  constructor(
    private hnAPI: HnAPIService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getId();
  }

  getId() {
    const onMapId = params => (params as any).id;

    this.route.params
      .map(onMapId)
      .subscribe(this.onGetId);
  }

  getItem(id) {
    return this.hnAPI.item(id)
      .subscribe(this.onGetItem);;
  }

  onGetId = (id) => this.getItem(id);

  onGetItem = (item) => this.item = item;
}
