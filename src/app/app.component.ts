import { Component, OnInit } from '@angular/core';

const template = require("pug!./app.component.pug");

@Component({
  selector: 'app-root',
  template: template(),
  styleUrls: ['./app.component.scss']
})

export class AppComponent { }