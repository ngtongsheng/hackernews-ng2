import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/';

const template = require("pug!./search.component.pug");

@Component({
  selector: 'search',
  template: template(),
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  control = new FormControl();

  @Output() change = new EventEmitter();
  @Input() search: string;

  ngOnInit() {
    this.getChange()
      .subscribe(this.onChange)
  }

  getChange() {
    return this.control.valueChanges
      .debounceTime(500)
      .distinctUntilChanged();
  }

  onChange = search => {
    if (search && search.type) {
      return;
    };
    this.change.emit(search);
  }
}
