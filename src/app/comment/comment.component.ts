import { Component, OnInit, Input } from '@angular/core';
import { DateService } from '../shared/date.service';

const template = require("pug!./comment.component.pug");

@Component({
  selector: 'comment',
  template: template(),
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() comment;

  constructor(
    private dateUtil: DateService
  ) { }

  ngOnInit() {
    this.comment.created = this.dateUtil.getDayAgo(new Date(this.comment.created_at));
  }

}
