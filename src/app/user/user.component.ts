import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HnAPIService } from '../shared/hn.api.service';
import { DateService } from '../shared/date.service';
import { Observable } from 'rxjs/';

const template = require("pug!./user.component.pug");

@Component({
    selector: 'user',
    template: template(),
    styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
    private user;

    constructor(
        private route: ActivatedRoute,
        private hnAPI: HnAPIService,
        private dateUtil: DateService,
    ) { }

    ngOnInit() {
        this.getName();
    }

    get paramsObservable() {
        return this.route.params
            .map(this.onMapName);
    }

    onMapName = params => (params as any).name;

    getName() {
        this.paramsObservable
            .subscribe(this.onGetName);
    }

    onGetName = name => this.getUser(name).subscribe(this.onGetUser);

    getUser = name => this.hnAPI.user(name).map(this.onMapUser);

    onMapUser = user => {
        user.updated = this.dateUtil.getDayAgo(new Date(user.updated_at));
        user.created = this.dateUtil.getDayAgo(new Date(user.created_at));
        return user;
    }

    onGetUser = user => this.user = user;
}
