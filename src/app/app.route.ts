import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { ItemComponent } from './item/item.component';
import { StoriesComponent } from './stories/stories.component';

const routes: Routes = [
  { path: 'hot/:page', component: StoriesComponent, data: { tag: 'hot' } },
  { path: 'show/:page', component: StoriesComponent, data: { tag: 'show' } },
  { path: 'ask/:page', component: StoriesComponent, data: { tag: 'ask' } },
  { path: 'comment/:page', component: StoriesComponent, data: { tag: 'comment' } },
  { path: 'user/:name', component: UserComponent },
  { path: 'item/:id', component: ItemComponent },
  { path: '**', redirectTo: 'hot/1' },
];

export const routing = RouterModule.forRoot(routes);