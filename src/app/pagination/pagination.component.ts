import { Component, Input, Output, EventEmitter } from '@angular/core';

const template = require("pug!./pagination.component.pug");

@Component({
  selector: 'pagination',
  template: template(),
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {

  num: number;
  @Input() max: number;
  @Input() min: number = 1;
  @Output() change = new EventEmitter();

  @Input()
  get page(): number {
    return this.num;
  }

  set page(val: number) {
    this.num = val;
    this.change.emit(this.num);
  }

  prev(event) {
    if (!this.isMin) this.page--;
  }

  next(event) {
    if (!this.isMax) this.page++;
  }

  get isMin() {
    return (this.page <= this.min);
  }

  get isMax() {
    return (this.page >= this.max);
  }
}
