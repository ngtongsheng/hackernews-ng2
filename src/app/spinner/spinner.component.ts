import { Component, Input } from '@angular/core';

const template = require("pug!./spinner.component.pug");

@Component({
  selector: 'spinner',
  template: template(),
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {

  @Input() show: boolean = true;

}
